﻿using NeuralNetStudio.NET.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NeuralNetStudio.NET.Models
{
    public class Dataset : RequestManager
    {
        public string Name { get; set; }
        public string Uuid { get; set; }
        public Dataset(UserParameters userParameters) : base(userParameters)
        { }
    }
}
