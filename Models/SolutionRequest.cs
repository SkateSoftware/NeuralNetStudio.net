﻿using NeuralNetStudio.NET.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace NeuralNetStudio.NET.Models
{
    public class SolutionRequest
    {
        public string Name;
        public Guid Guid;
        public Category Category;
        public Task Task;
        public DateTime RequestedOn;
    }
}
