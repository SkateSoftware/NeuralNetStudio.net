﻿using NeuralNetStudio.NET.Inference;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Text;
using Microsoft.Win32.SafeHandles;
using NeuralNetStudio.NET.Auth;
using NeuralNetStudio.NET.Core;
using Newtonsoft.Json.Linq;
using Org.OpenAPITools.Api;
using Org.OpenAPITools.Client;
using Org.OpenAPITools.Model;


using OpenApiConfig = Org.OpenAPITools.Client.Configuration;
using NeuralNetStudio.NET.Models;
using AutoMapper;

namespace NeuralNetStudio.NET
{
    public class Solution : Core.RequestManager
    {
        public string name;
        public Guid uuid;
        public NetworkCategory category;
        public NetworkTask task;
        public List<Model> models;
        public List<Dataset> datasets;
        public Guid DefaultModelUuid;
        private IMapper mapper = null;

        public ModelRequest BuildModel(Hyperparameters hyperparameters)
        {
            throw new NotImplementedException();
        }

        //internal Solution(string name, string uuid, List<Model> models, UserParameters userParameters) : base(userParameters)
        internal Solution(UserParameters userParameters = null) : base(userParameters)
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<PredictionModel, Prediction>();
            });
            this.mapper = config.CreateMapper();
        }
        internal Solution(string name, Guid uuid, List<Model> models, Guid defaultModelUuid=new Guid(), UserParameters userParameters=null) : base(userParameters)
        {
            this.name = name;
            this.uuid = uuid;
            //this.model_request_uuid = model_request_uuid;
            this.DefaultModelUuid = defaultModelUuid;
            this.models = models;
        }

        public Prediction Predict(string inputFile)
        {
            var inputData = Utils.CsvUtils.CsvToJson(inputFile);
            Guid modelUuid = this.DefaultModelUuid;
            var result = this.apiInstance.PredictTabularModelsModelUuidTabularPredictionPost(modelUuid.ToString(), inputData);
            //var obj = this.apiInstance.PredictModelsModelUuidPredictionPost(modelUuid, inputFile: fileStream);

            Prediction retVal = this.mapper.Map<Prediction>(result);
            return retVal;
        }

        public Model DefaultModel
        {
            get
            {
                ModelModel model = this.apiInstance.GetModelModelModelUuidGet(this.DefaultModelUuid.ToString());
                Model retVal = this.mapper.Map<Model>(model);
                return retVal;
            }
        }
        public string Name   // property
        {
            get { return this.name; }   // get method
            //set { name = value; }  // set method
        }

        public Guid Uuid   // property
        {
            get { return this.uuid; }   // get method
            //set { name = value; }  // set method
        }

        //public string ModelRequestUuid   // property
        //{
        //    get { return this.model_request_uuid; }   // get method
            //set { name = value; }  // set method
        //}

        public List<Model> Models { get; set; }

        public List<Dataset> Datasets { get; set; }

        public Prediction RunModel(string inputFile, string modelUuid)
        {
            //OpenApiConfig config = new OpenApiConfig();
            //config.AccessToken = this.apiClient.GetToken();
            //ar apiInstance = new DefaultApi(config);
            string className = "";
            int probability = 88;
            using (FileStream fileStream = new FileStream(inputFile, FileMode.Open))
            {
                var obj = this.apiInstance.PredictModelsModelUuidPredictionPost(modelUuid, inputFile: fileStream);
                Console.WriteLine(obj);
                //className = obj["Classname"];
                //probability = obj["Probability"];
            }

            Prediction retVal = new Prediction(className, probability);
            return retVal;
        }

    }
}
