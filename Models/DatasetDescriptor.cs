﻿using NeuralNetStudio.NET.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace NeuralNetStudio.NET.Models
{
    class DatasetDescriptor
    {
        public Category Category;
        public Task Task;
        public string Name;
    }

    class TabularDatasetDescriptor
    {
        public string DependentVariable;
    }
}
