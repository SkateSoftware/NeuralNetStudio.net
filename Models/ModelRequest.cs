﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NeuralNetStudio.NET.Models
{
    public class ModelRequest
    {
        public string Name;
        public Guid Guid;
        public Guid SolutionGuid;
        public DateTime RequestedOn;
    }
}
