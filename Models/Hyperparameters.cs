﻿using NeuralNetStudio.NET.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace NeuralNetStudio.NET.Models
{
    public class Hyperparameters
    {
        public int Epochs;
        public int BatchSize;
        public List<Metric> Metrics;
        
        public Hyperparameters(int epochs=6, int batchSize=32, List<Metric> metrics=null)
        {
            Epochs = epochs;
            BatchSize = batchSize;
            Metrics = metrics;
        }
    }
}
