﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NeuralNetStudio.NET.Inference
{
    public class Prediction
    {
        public Prediction()
        {
        }
        public Prediction(string value, int probability)
        {
            this.Value = value;
            this.Probability = probability;
        }

        public string Value { get; }
        public int Probability { get; }
    }
}
