﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Text;
using Microsoft.Win32.SafeHandles;
using NeuralNetStudio.NET.Auth;
using NeuralNetStudio.NET.Core;
using NeuralNetStudio.NET.Inference;
using NeuralNetStudio.NET.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Org.OpenAPITools.Api;
using Org.OpenAPITools.Client;
using Org.OpenAPITools.Model;
using AutoMapper;

using OpenApiConfig = Org.OpenAPITools.Client.Configuration;

namespace NeuralNetStudio.NET
{
    public class SolutionManager : RequestManager
    {
        private IMapper mapper;
        public SolutionManager(UserParameters userParameters) : base(userParameters)
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<string, Guid>().ConvertUsing(s => Guid.Parse(s));
                cfg.CreateMap<SolutionModel, Solution>().ConstructUsing(x => new Solution(this.userParameters));
                cfg.CreateMap<ModelModel, Model>().ConstructUsing(x => new Model(this.userParameters));
                cfg.CreateMap<DatasetModel, Dataset>().ConstructUsing(x => new Dataset(this.userParameters));
                cfg.CreateMap<SolutionRequestModel, SolutionRequest>()
                    .ForMember(dest => dest.Guid,
                    opts => opts.MapFrom(src => src.Uuid));
            });
            this.mapper = config.CreateMapper();
        }           
        
        public List<Solution> GetSolutions()
        {
            //Object response = this.apiInstance.GetSolutionsSolutionsGet();
            List<SolutionModel> response = this.apiInstance.GetSolutionsSolutionsGet();
            
            List<Solution> retVal = new List<Solution>();
            foreach (SolutionModel solutionModel in response)
            {
                Solution solution = this.mapper.Map<Solution>(solutionModel);
                retVal.Add(solution);
            }
            return retVal;
        }

        public Solution Create(string name, string datasetUuid)
        {

            Solution retVal = null;
            return retVal;
        }

        /*
         * If datasetName is not specified, we will used the filename.
         * If Category is not specified:
         *  1. csv = Tabular
         * If Task is not specified:
         *  1. DependentVariable.unique() = 2 => BinaryClassifier
         *  2. DependentVariable.unique()/count > .3 => Classifier
         *  1. else => Regression
        */
        public SolutionRequest CreateTabular(string name, string datasetFilePath, string dependentVariable, Hyperparameters hyperparameters=null, string datasetName = null, Category category = Category.Tabular, Task task = Task.Empty)
        {
            SolutionRequest retVal = null;
            if(datasetName == null)
            {
                datasetName = Path.GetFileNameWithoutExtension(datasetFilePath);                
            }

            using (FileStream fileStream = new FileStream(datasetFilePath, FileMode.Open))
            {
                NetworkTask networkTask = (NetworkTask)Enum.Parse(typeof(NetworkTask), task.ToString());
                //DatasetDescriptorModel dataseteDescriptor = new DatasetDescriptorModel(name = datasetName);//, category = NetworkCategory.Tabular); //, category= NetworkCategory.Tabular, task= NetworkTask.Empty);
                //dataseteDescriptor.category = NetworkCategory.Tabular;


                Hyperparameters h = new Hyperparameters(epochs: 7, batchSize: 33, metrics: new List<Metric>(new Metric[] { Metric.Accuracy }));
                string h_json = JsonConvert.SerializeObject(h);

                var solutionRequestModel = this.apiInstance.CreateSolutionSolutionsPost(solutionName:name, 
                    category: NetworkCategory.Tabular, task: NetworkTask.Empty, 
                    datasetName: datasetName, hyperparameters: h_json,
                    datasetCategoryDescriptor: "CategoryDescriptor", dataFile: fileStream);
                retVal = this.mapper.Map<SolutionRequest>(solutionRequestModel);
            }
            return retVal;
        }
       public Solution Create(string name, string datasetFilePath, string dependentVariable = null, string datasetName = null, Category category = Category.Empty, Task task = Task.Empty)
       {
            string modelName = $"{name}Model";

            Solution retVal = null;
            using (FileStream fileStream = new FileStream(datasetFilePath, FileMode.Open))
            {
                //this.apiInstance.CreateSolutionSolutionsPost
                object objSolution = null; // this.apiInstance.CreateSolutionSolutionsPost(name, modelName, category, task, dataFile: fileStream);

                Console.WriteLine(objSolution);
                JObject jObjSolution = objSolution as JObject;
                string status = (string)jObjSolution["status"];
                if (status != "Success")
                {
                    string error = (string)jObjSolution["value"];
                    throw new Exception(error);
                }
                JObject value = jObjSolution["value"] as JObject;
                //Solution solution = jObjSolution.ToObject<Solution>();
                string solution_uuid = (string)value["solution_uuid"];
                string model_request_uuid = (string)value["model_request_uuid"];
                object obj = apiInstance.GetSolutionSolutionsSolutionUuidGet(solution_uuid);
                SolutionModel solutionModel = obj as SolutionModel;
            }
            // ToDo: Map!
            List<Model> models = new List<Model>();
            Solution ret_val = null; // new Solution(solutionModel.Name, solutionModel.Uuid, models, userParameters: this.userParameters);
            return ret_val;
        }

        public Prediction RunModel(string modelUuid, string inputFile)
        {
            //OpenApiConfig config = new OpenApiConfig();
            //config.AccessToken = this.apiClient.GetToken();
            //var apiInstance = new DefaultApi(config);
            string className = "";
            int probability = 88;
            using (FileStream fileStream = new FileStream(inputFile, FileMode.Open))
            {   
                var obj = this.apiInstance.PredictModelsModelUuidPredictionPost(modelUuid, inputFile: fileStream);
                Console.WriteLine(obj);
                //className = obj["Classname"];
                //probability = obj["Probability"];
            }
            
            Prediction retVal = new Prediction(className, probability);
            return retVal;
        }

        public Solution Get(Guid solutionUuid)
        {
            var obj = this.apiInstance.GetSolutionSolutionsSolutionUuidGet(solutionUuid.ToString());

            Solution retVal = this.mapper.Map<Solution>(obj);
            return retVal;
        }

        public Solution Get(string solutionName)
        {
            var result = this.apiInstance.GetSolutionSolutionsSolutionUuidGet(solutionName);
            Solution retVal = null;
            return retVal;
        }
    }
}
