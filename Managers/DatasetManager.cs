﻿using NeuralNetStudio.NET.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Org.OpenAPITools.Api;
using Org.OpenAPITools.Client;
using Org.OpenAPITools.Model;

using OpenApiConfig = Org.OpenAPITools.Client.Configuration;
using NeuralNetStudio.NET.Core;

namespace NeuralNetStudio.NET.Managers
{
    public class DatasetManager : RequestManager
    {
        public DatasetManager(UserParameters userParameters) : base(userParameters)
        {
        }

        public List<Dataset> GetDatasets()
        {
            var datasets = this.apiInstance.GetDatasetsDatasetsGet();
            List<Dataset> retVal = datasets as List<Dataset>;
            return retVal;
        }
    }
}
