﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NeuralNetStudio.NET.Utils
{
    class CsvUtils
    {
        public static List<Dictionary<string, object>> ReadCsv(string inputPath)
        {
            //var dict = File.ReadLines(inputPath).
            //    Select(line => line.Split(',')).ToDictionary(line => line[0], line => line[1]);
            //var dict = File.ReadLines(inputPath).Select(line => line);

            List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();

            using (StreamReader reader = new StreamReader(inputPath))
            {
                string[] header = null;
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    string[] parts = line.Split(',');
                    if (header == null)
                    {
                        header = parts;
                        continue;
                    }
                    Dictionary<string, object> row = new Dictionary<string, object>();
                    for (int i = 0; i < parts.Length; i++)
                    {
                        row.Add(header[i], parts[i]);
                    }
                    data.Add(row);
                }
            }
            return data;
        }

        public static string CsvToJson(string csvPath)
        {
            List<Dictionary<string, object>> data = ReadCsv(csvPath);

            var row = data[0];
            string row_json = JsonConvert.SerializeObject(row, Formatting.Indented);
            return row_json;
        }
    }
}
