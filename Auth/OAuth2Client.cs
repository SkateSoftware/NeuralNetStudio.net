﻿using IdentityModel.Client;
using System;
using System.Net.Http;
using System.Runtime.CompilerServices;

namespace NeuralNetStudio.NET.Auth
{
    public class OAuth2Client
    {
        public string TokenUrl;
        public string ClientId;
        public string ClientSecret;
        public string ApiScopes;

        public OAuth2Client(string tokenUrl, string clientId, string clientSecret, string apiScopes)
        {
            this.TokenUrl = tokenUrl;
            this.ClientId = clientId;
            this.ClientSecret = clientSecret;
            this.ApiScopes = apiScopes;
        }
        public string GetToken()
        { 
            HttpMessageInvoker invoker = new HttpClient();
            TokenClientOptions options = new TokenClientOptions
            {
                Address = this.TokenUrl,
                ClientId = this.ClientId,
                ClientSecret = this.ClientSecret
            };
            var client = new TokenClient(invoker, options);
            var response = client.RequestClientCredentialsTokenAsync(this.ApiScopes).Result;
            if(response.HttpStatusCode != System.Net.HttpStatusCode.OK)
            {
                throw new System.Exception($"Failed to get token.  Error {response.HttpErrorReason}");
            }
        String token = response.AccessToken;
                return token;
        }
    }
}
