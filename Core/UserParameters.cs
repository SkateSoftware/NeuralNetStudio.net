﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NeuralNetStudio.NET.Core
{
    public class UserParameters
    {
        public string clientId;
        public string clientSecret;
        public string apiScopes;
        public string apiKey;

        public UserParameters(string clientId, string clientSecret, string apiScopes, string apiKey)
        {
            this.clientId = clientId;
            this.clientSecret = clientSecret;
            this.apiScopes = apiScopes;
            this.apiKey = apiKey;
        }
    }
}
