﻿

using System;
using System.Collections.Generic;
using System.Text;

namespace NeuralNetStudio.NET.Core
{
    public enum VisionClassificationSolutions
    {
        BirdsOfNewEngland,
        FlowersOfNewEngland,
        FreshwaterFishOfNorthAmerica
    }
}
