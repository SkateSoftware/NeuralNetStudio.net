﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NeuralNetStudio.NET.Core
{
    public enum Task
    {
        Empty = 0,
        BinaryClassifier = 1,
        Classifier = 2,
        Regression = 3,
        ObjectDetection = 4
    }
}
