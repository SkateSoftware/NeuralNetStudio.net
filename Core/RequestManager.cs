﻿using NeuralNetStudio.NET.Auth;
using Org.OpenAPITools.Api;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


using OpenApiConfig = Org.OpenAPITools.Client.Configuration;

namespace NeuralNetStudio.NET.Core
{
    /*
     * NnsApiClient 
     * 1. gets token 
     * 2. holds instance of ApiInstance
     */
    public class RequestManager
    {
        public UserParameters userParameters;
        public DefaultApi apiInstance;
        private OAuth2Client oAuth2Client;
        public RequestManager(UserParameters userParameters)
        {
            this.userParameters = userParameters;
            string tokenUrl = ConfigurationManager.AppSettings["tokenUrl"];
            tokenUrl = this.GetTokenUrl();
            this.oAuth2Client = new OAuth2Client(tokenUrl, this.userParameters.clientId, 
                this.userParameters.clientSecret,this.userParameters.apiScopes);

            OpenApiConfig config = new OpenApiConfig();
            config.AccessToken = this.oAuth2Client.GetToken();
            config.BasePath = "https://api.neuralnetstudio.com";
            config.BasePath = "http://wakhanthanka:8000";

            this.apiInstance = new DefaultApi(config);
        }

        private string GetTokenUrl()
        {
            Configuration config = null;
            string exeConfigPath = this.GetType().Assembly.Location;
            try
            {
                config = ConfigurationManager.OpenExeConfiguration(exeConfigPath);
            }
            catch (Exception ex)
            {
                //handle errror here.. means DLL has no sattelite configuration file.
            }
            string retVal = config.AppSettings.Settings["tokenUrl"].Value;
            return retVal.ToString();
        }

        public string GetToken()
        {
            string accessToken = this.oAuth2Client.GetToken();
            return accessToken;
        }

    }
}
