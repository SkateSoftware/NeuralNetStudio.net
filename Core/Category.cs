﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NeuralNetStudio.NET.Core
{
    public enum Category
    {
        Empty = 0,
        Vision = 1,
        Text = 2,
        Tabular = 3,
        Generative = 4,
        ReinforcementLearning = 5
    }
}
