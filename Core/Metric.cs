﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Text;

namespace NeuralNetStudio.NET.Core
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum Metric
    {
        Accuracy = 1,
        ROC = 2,
        AUROC = 3,
        RMSE = 4,
        RMSLE = 5
    }
}
