﻿using Newtonsoft.Json;
using Org.OpenAPITools.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NeuralNetStudio.NET.Core
{
    public class Utils
    {
        public static string Serialize(object obj)
        {
            string value = "";
            try
            {
                value = obj != null ? JsonConvert.SerializeObject(obj) : null;
            }
            catch (Exception e)
            {
                throw new ApiException(500, e.Message);
            }
            string ret_val = value.Replace("\"", "");
            return ret_val;
        }
    }
}
